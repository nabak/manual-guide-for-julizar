[[_TOC_]]

## Cara Backup Data

Backup dilakukan secara otomatis menggunakan **automatic snapshot** dari amazon, jika perlu backup juga bisa dilakukan secara manual.
Backup akan dilakukan pada jam 00:00 GMT+7 setiap harinya, tapi tergantung situasi mungkin akan dilakukan backup dengan frekuensi yang lebih sering lagi seiring perkembangan web dan tingkat aktifitas user guna mencegah kehilangan data.
Data yang dibackup adalah keseluruhan isi disk.
Proses backup tergolong cepat yang berkisar 3-5 menit.
Berikut contoh dari hasil snapshot.

![Automatic Snapshot Amazon](./snapshot.png)

## Cara Maintenance Server

Julizar menggunakan server dari amazon lightsail sehingga tidak memerlukan maintenance ataupun perawatan dari segi hardware.
Untuk maintenance hanya update software seperti web server (Nginx), PHP, Node.js dan software-software pendukung lainnya guna menjaga kestabilan sistem.

## Cara Pembuatan akun customer

Untuk membuat akun customer bisa melalui halaman https://julizar.com/id/auth?tab=register dengan mengisi beberapa form seperti username, email dan password.

## Cara Pembuatan akun Admin

Admin terdiri dari 2 role **super admin** dan **writer**, akun super admin dibuat secara manual melalui CLI dan hanya tersedia 1 akun yang bisa mempunyai akses super admin.
Sedangkan role writer bisa dibuat oleh super admin.
Role writer hanya mempunyai hak akses untuk membuat/mengedit/menghapus artikel.
Halaman admin ada di https://blueprint.julizar.com dan tidak digabung dengan halaman customer dengan alasan security & untuk mempermudah maintenance.

## Cara restore data/backup

Karena Julizar menggunakan automatic snapshot dari amazon untuk restore data hanya perlu buat instance/server baru menggunakan hasil dari snapshot.
Proses ini biasanya hanya memakan waktu tidak lebih dari 5 menit.

## Penanganan gangguan server atau web service

Untuk menangani masalah ini kurang lebih akan dilakukan penanganan sebagai berikut, tergantung jenis gangguan dan masalah yang terjadi.

1. membuat server baru dengan hasil backup snapshot kemudian mengalihkan domain ke server baru tersebut sampai web/server asli kembali normal.
2. membatasi akses halaman atau fitur tertentu pada website sementara web diperbaiki.
3. membuat server down secara keseluruhan dan hanya bisa diakses oleh developer.

## deployment website

Julizar menyimpan keseluruhan script di git.
Setelah di upload ke git server akan otomatis update dan build script dari git kemudian update script lama dengan yang baru jika build berhasil.

NOTE: build hanya berlaku untuk frontend sedangkan untuk web API/backend tidak memerlukan build untuk deployment.
